Description:
Apps are the pets for a generation of children raised on tech and tablets. Our team wanted to enable Tablets / mobile to engage in real and personalized conversation with children and evolve with the kid as he or she grows with the power of Watson services on Bluemix platform. We bring gadgets to life with speech and a personality, allowing them to interactively engage and help in the growth of a child. This mobile application is designed to aid kids development, allowing them to take a picture of an object or upload an image and get an apt reply in matter of seconds.
Key Features:

The current prototype has following key features 
1. A simple and intuitive design that allows a kid to capture an object and ask Watson about it  and the Watson responds with the object name ,description and related images.
2. The current design is aimed at children aged four to seven, and the system can be customized to include information such as the child's name and age.
3. This application uses Bluemix and Visual Recognition, Concept Insights ,Speech to Text, Text to Speech, Alchemy Language, Language Translation services of Watson.
4. This supports both iOS and Andriod platforms.
5. Currently having language support for English, Spanish, Japanese
6. The app is designed to give response in swift time.
7. The future of this app has features to include speech recognition which enables kids to chat with the app and answers a wide variety of who, when, why, where questions.

App is hosted on: http://mywalle.mybluemix.net/

mywalle.apk is there along with the source files
package com.ibm.talenthunter.servlets;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;

import com.ibm.talenthunter.util.DbHelper;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/**
 * Servlet implementation class Employees
 */
@WebServlet("/Employees")
public class Employees extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Employees() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		org.json.JSONObject employees=null;
		//String result="{Hello:123}";
		
		//Get Connection
		DB db = DbHelper.getDbConnection();
		//Query object
		  DBCollection table = db.getCollection("TalentHunter");
	      
		DBCollection employeeTable = db.getCollection("Employee");
		DBCollection skillsTable = db.getCollection("Skills");
		DBCollection projectTable = db.getCollection("Projects");
		
		
		BasicDBObject skillsArr[] = new BasicDBObject[5];
		skillsArr[0] = new BasicDBObject();
		skillsArr[0].put("id", 101);
		skillsArr[0].put("name", "java");
		
		skillsArr[1] = new BasicDBObject();
		skillsArr[1].put("id", 101);
		skillsArr[1].put("name", "C++");
		skillsTable.insert(skillsArr[0]);
		skillsTable.insert(skillsArr[1]);
		
		BasicDBObject projects[] = new BasicDBObject[5];
		projects[0] = new BasicDBObject();
		projects[0].put("id", 101);
		projects[0].put("name", "TCP");
		projects[0].put("details", "bla bla bla");
		
		projects[1] = new BasicDBObject();
		projects[1].put("id", 102);
		projects[1].put("name", "Calvin Klien");
		projects[1].put("details", "bla bla bla");
		projectTable.insert(projects[0]);
		projectTable.insert(projects[1]);
		
		BasicDBObject personalitys[] = new BasicDBObject[5];
		personalitys[0]=new BasicDBObject();
		personalitys[0].put("xyz",40);
		personalitys[0].put("abc",60);
		personalitys[0].put("poi",89);
		personalitys[0].put("mnb",78);
		personalitys[0].put("kjh",68);
		
		BasicDBObject emplyee = new BasicDBObject();
		emplyee.put("id", 1002);
		emplyee.put("name", "yash");
		emplyee.put("picurl", "http://pic.jpg");
		emplyee.put("skills", skillsArr);
		emplyee.put("projects", projects);
		emplyee.put("personalityscore", personalitys);
		employeeTable.insert(emplyee);

       
		
        BasicDBObject searchQuery2 = new BasicDBObject().append("id", 1001);

        
        DBCursor cursor2 = employeeTable.find(searchQuery2);
        DBObject result=null;
        while (cursor2.hasNext()) {
        	result=cursor2.next();
        	System.out.println( "found: " + result);
      //  }
		//try {
			System.out.println("converting...");
			try {
				employees = new org.json.JSONObject(result.toString());
			} catch (JSONException e) {
				// generated catch block
				e.printStackTrace();
			}
			System.out.println("converted..."+employees);
        }
		//} catch (JSONException e) {
			// TODO Auto-generated catch block
	//		e.printStackTrace();
	//	}
		response.setContentType("application/json");
		response.getWriter().write(employees.toString());
	}

}

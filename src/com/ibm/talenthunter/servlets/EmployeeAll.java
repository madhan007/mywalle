package com.ibm.talenthunter.servlets;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;

import com.ibm.talenthunter.util.DbHelper;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/**
 * Servlet implementation class Employees
 */
@WebServlet("/EmployeeAll")
public class EmployeeAll extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EmployeeAll() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		JSONArray employees=null;
		//String result="{Hello:123}";
		
		//Get Connection
		DB db = DbHelper.getDbConnection();
		//Query object	  
		DBCollection employeeTable = db.getCollection("Employee");

		
        DBCursor cursor2 = employeeTable.find();
        DBObject result=null;
        String jsonString="[";
        response.getWriter().write("[");
        int resultcount=1;
        
        while (cursor2.hasNext()) {
        	result=cursor2.next();
        //	System.out.println( "found: " + result);
      //  }
		//try {
			System.out.println("converting...");
				if(jsonString.length()<2){
					jsonString=jsonString+result.toString();
					response.getWriter().write(result.toString());
				}
				else{
					jsonString=jsonString+","+result.toString();
					response.getWriter().write(","+result.toString());
				}
				
			
		//	System.out.println("converted..."+employees);
        }
        jsonString=jsonString+"]";
        System.out.println("converted..."+jsonString);
        try {
			employees = new org.json.JSONArray(jsonString.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
     
		response.setContentType("application/json");
		if(employees!=null)
			response.getWriter().write(employees.toString());
		else
			response.getWriter().write("notfound");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

}

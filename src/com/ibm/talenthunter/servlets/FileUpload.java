package com.ibm.talenthunter.servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class FileUpload
 */
@WebServlet("/FileUpload")
public class FileUpload extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
  private final String UPLOAD_DIRECTORY = "/app/wlp/usr/servers/defaultServer/apps";
    public FileUpload() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Credentials", "true");
		
	    response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
        response.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
        response.addHeader("Access-Control-Max-Age", "1728000");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
		response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
		 PrintWriter out = response.getWriter();
		 String name="myimg.jpg";
		   JSONObject json = new JSONObject();
		   JSONObject infoobj;
		   JSONObject errobj=new JSONObject();
	     //process only if its multipart content
        if(ServletFileUpload.isMultipartContent(request)){
            try {
            	
                List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
                
                for(FileItem item : multiparts){
                    if(!item.isFormField()){
                        name = new File(item.getName()).getName();
                        
                        item.write( new File(UPLOAD_DIRECTORY + File.separator + name));
                        
                        
                    }
                }
                
                  //create Json Object
                
                errobj=new JSONObject ();
                errobj.put("status", "Please try again later");
                 System.out.println("filename:"+name);
                 
                  infoobj= AlchemyImage.getImageKeyword(name);
                  if(infoobj==null)
                  {
                	
                	  infoobj.put("status", "File Uploaded Successfully");
                	  infoobj.put("keyword", "Not found");
                  }
                  else
                  {
                	  infoobj.put("status", "File Uploaded Successfully");
                	 
                  }
                   System.out.println("infoobj :"+infoobj.toString());

                      out.println(infoobj);
      
            } catch (Exception ex) {
            	
            	System.out.println("Exception: "+ex);
                 
              /*  try {
					errobj.put("Exception", ex);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
                out.println(errobj.toString());
            }
         
        }else{
        	
        	 out.println(errobj);
        }
    
	}

}

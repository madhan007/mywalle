package com.ibm.talenthunter.servlets;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;

import com.ibm.talenthunter.util.DbHelper;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/**
 * Servlet implementation class Employees
 */
@WebServlet("/FindEmployee")
public class FindEmployee extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FindEmployee() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		org.json.JSONObject employees=null;
		//String result="{Hello:123}";
		
		//Get Connection
		DB db = DbHelper.getDbConnection();
		//Query object
		  
		DBCollection employeeTable = db.getCollection("Employee");
		int empid=Integer.parseInt(request.getParameter("id"));
		
        BasicDBObject searchQuery2 = new BasicDBObject().append("id", empid);

        
        DBCursor cursor2 = employeeTable.find();
        DBObject result=null;
        while (cursor2.hasNext()) {
        	result=cursor2.next();
        	System.out.println( "found: " + result);
      //  }
		//try {
			System.out.println("converting...");
			try {
				employees = new org.json.JSONObject(result.toString());
			} catch (JSONException e) {
				// generated catch block
				e.printStackTrace();
			}
			System.out.println("converted..."+employees);
        }
		//} catch (JSONException e) {
			// TODO Auto-generated catch block
	//		e.printStackTrace();
	//	}
		response.setContentType("application/json");
		if(employees!=null)
			response.getWriter().write(employees.toString());
		else
			response.getWriter().write("notfound");
	}

}
